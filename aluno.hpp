#ifndef ALUNO_HPP
#define ALUNO_HPP

#include <iostream>
#include "pessoa.hpp"

class Aluno : public Pessoa {
	// Atributos
	private: 
		int matricula_aluno;
		int semestre;
		float ira;
	public:
		Aluno();
		~Aluno();
		void setMatriculaAluno(int matricula);
		void setSemestre(int semestre);
		void calculaIra();
		int getMatriculaAluno();
		int getSemestre();
		float getIra();
};

#endif